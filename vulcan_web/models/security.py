# Core Imports
from flask_security import UserMixin, RoleMixin

# App Imports
from vulcan_web.models.helpers import DefaultMixin
from vulcan_web.extensions import db

user_roles = db.Table('user_roles', db.Model.metadata,
                      db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
                      db.Column('role_id', db.Integer, db.ForeignKey('roles.id')))


class Role(db.Model, DefaultMixin, RoleMixin):
    __tablename__ = "roles"

    class Meta:
        verbose_name = 'role'
        verbose_name_plural = 'roles'
        ordering = ('name',)

    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, DefaultMixin, UserMixin):
    __tablename__ = "users"

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        ordering = ('email',)

    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime)

    timezone = db.Column(db.String(45), nullable=False, default='CET')
    locale = db.Column(db.String(45), nullable=False, default='sl_si')

    roles = db.relationship('Role', secondary=user_roles,
                            backref=db.backref('users', lazy='dynamic'))
