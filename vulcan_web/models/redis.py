"""Redis keys used throughout the entire application (Flask, etc.)."""

# Email throttling.
EMAIL_THROTTLE = 'vulcan_web:email_throttle:{md5}'  # Lock.
