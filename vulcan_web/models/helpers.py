"""Convenience functions which interact with SQLAlchemy models."""
import copy
from sqlalchemy import func
from sqlalchemy import Column, Integer
from sqlalchemy.sql import operators
from sqlalchemy.orm.attributes import manager_of_class
from sqlalchemy.orm.properties import ColumnProperty

from flask_babel import gettext as _

from vulcan_web.extensions import db

QUERY_SEP = "__"

_oper = {
    'eq': operators.eq,
    'gt': operators.gt,
    'lt': operators.lt,
    'ge': operators.ge,
    'le': operators.le,
    'ne': operators.ne,
    'contains': operators.contains_op,
}


class DefaultMixin(object):
    """Convenience base DB model class. Makes sure tables in MySQL are created as InnoDB.
    This is to enforce foreign key constraints (MyISAM doesn't support constraints) outside of production.
    """
    def __getstate__(self):
        cls = type(self)
        mgr = manager_of_class(cls)
        return dict((key, getattr(self, key))
                    for key, attr in mgr.iteritems()
                    if isinstance(attr.property, ColumnProperty))

    def __setstate__(self, state):
        self.__dict__.update(state)

    __table_args__ = dict(mysql_charset='utf8', mysql_engine='InnoDB')

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)

    @classmethod
    def get_or_create(cls, defaults=None, **kwargs):
        if not defaults:
            defaults = {}

        obj = cls.query.filter_by(**kwargs).first()

        if obj:
            return obj, False
        else:
            defaults.update(kwargs)
            obj = cls(**defaults)

            db.session.add(obj)

            return obj, True


def count(column, value, glob=False):
    """Counts number of rows with value in a column. This function is case-insensitive.

    Args:
        column: -- the SQLAlchemy column object to search in (e.g. Table.a_column).
        value: -- the value to search for, any string.
        glob: -- enable %globbing% search (default False).

    Returns:
    Number of rows that match. Equivalent of SELECT count(*) FROM.
    """
    query = db.session.query(func.count('*'))
    if glob:
        query = query.filter(column.ilike(value))
    else:
        query = query.filter(func.lower(column) == value.lower())
    return query.one()[0]


def filter_query(query_obj, **filters):
    for name, value in filters.iteritems():
        col = None
        joined = None
        op = _oper['eq']
        tokens = name.split(QUERY_SEP)
        for tok in tokens:
            if tok in _oper:
                if col is None:
                    raise ValueError(_("No column supplied"))
                op = _oper[tok]
            else:
                mapper = query_obj._primary_entity.mapper
                if joined:
                    mapper = joined.mapper
                col = getattr(mapper.columns, tok, None)
                if col is None:
                    if not joined:
                        joined = mapper.relationships[tok]
                        if joined:
                            query_obj = query_obj.join(tok)
                    else:
                        raise AttributeError(_("Unknown field: '%s'") % name)
                else:
                    joined = None

        query_obj = query_obj.filter(op(col, value))

        query_obj = query_obj.reset_joinpoint()

    return query_obj


def deep_parse_post(key, post):
    if isinstance(post, str):
        return post
    elements = []
    for item in post.iteritems():
        if item[0].startswith(key):
            elements.append(item)

    parsedl = []
    parsedd = {}
    for postkey, postvalue in elements:
        parsekey = postkey[len(key):]
        if len(parsekey) == 0:
            parsedl.append(postvalue)
        else:
            if parsekey[0] != "[":
                continue

            level = 0
            levels = {}
            curs = ""
            for l in parsekey:
                if l == "[":
                    pass
                elif l == "]":
                    levels[level] = curs
                    curs = ""
                    level += 1
                else:
                    curs += l
            curd = parsedd
            for i in range(level):
                curkey = levels[i]
                if not curd.has_key(curkey):
                    if i == level-1:
                        curd[curkey] = postvalue
                    else:
                        curd[curkey] = {}
                curd = curd[curkey]

    # TODO: Jagged arrays / bare values
    return parsedd
