# SQLAlchemy Imports
from sqlalchemy import func, select
from sqlalchemy.orm import column_property

# App Imports
from vulcan_web.libs import WEIGHT_DM
from vulcan_web.models.helpers import DefaultMixin
from vulcan_web.extensions import db


class Status(db.Model, DefaultMixin):
    __tablename__ = "statuses"

    class Meta:
        verbose_name = 'status'
        verbose_name_plural = 'statuses'
        ordering = ('name',)

    name = db.Column(db.String(45), nullable=False, unique=True)

    def __repr__(self):
        return "vulcan.Status: %s" % self.name


class Cut(db.Model, DefaultMixin):
    __tablename__ = "cuts"

    class Meta:
        verbose_name = 'cut'
        verbose_name_plural = 'cuts'
        ordering = ('cut_at',)

    cut_at = db.Column(db.TIMESTAMP, onupdate=db.func.utc_timestamp())

    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    status = db.relationship('Status', backref=db.backref('cuts'))

    user_id = db.Column(db.Integer, db.ForeignKey('users.id', onupdate='SET NULL', ondelete='SET NULL'))
    user = db.relationship('User', backref=db.backref('cuts'))

    def __repr__(self):
        return "vulcan.Cut: %d" % self.id


class Quality(db.Model, DefaultMixin):
    __tablename__ = "qualities"

    class Meta:
        verbose_name = 'quality'
        verbose_name_plural = 'qualities'
        ordering = ('name',)

    name = db.Column(db.String(45), nullable=False, unique=True)
    place = db.Column(db.String(45))
    cut_time = db.Column(db.Integer)

    def __repr__(self):
        return "vulcan.Quality: %d" % self.id


class Order(db.Model, DefaultMixin):
    __tablename__ = "orders"

    class Meta:
        verbose_name = 'order'
        verbose_name_plural = 'orders'
        ordering = ('order_number',)

    order_number = db.Column(db.Integer, nullable=False)
    description = db.Column(db.TEXT)
    entered_at = db.Column(db.TIMESTAMP, onupdate=db.func.utc_timestamp())

    def __repr__(self):
        return "vulcan.Order: %d" % self.id


class OrderDeliverable(db.Model, DefaultMixin):
    __tablename__ = "order_deliverables"

    class Meta:
        verbose_name = 'order deliverable'
        verbose_name_plural = 'order deliverables'
        ordering = ('name',)

    x = db.Column(db.Integer, nullable=False)
    y = db.Column(db.Integer, nullable=False)
    z = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(45))
    amount = db.Column(db.Integer, nullable=False, default='1')

    order_id = db.Column(db.Integer, db.ForeignKey('orders.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    order = db.relationship('Order', backref=db.backref('order_deliverables',
                                                        cascade="all, delete-orphan",
                                                        passive_deletes=True))

    quality_id = db.Column(db.Integer, db.ForeignKey('qualities.id'), nullable=False)
    quality = db.relationship('Quality', backref=db.backref('order_deliverables'))

    def __repr__(self):
        return "vulcan.OrderDeliverable: %d" % self.id


class OrderDeliverableCut(db.Model, DefaultMixin):
    __tablename__ = "order_deliverable_cuts"

    class Meta:
        verbose_name = 'order deliverable cut'
        verbose_name_plural = 'order deliverable cuts'
        ordering = ('amount',)

    amount = db.Column(db.Integer, nullable=False, default='1')

    order_deliverable_id = db.Column(db.Integer,
                                     db.ForeignKey('order_deliverables.id', onupdate='CASCADE', ondelete='CASCADE'),
                                     nullable=False)
    order_deliverable = db.relationship('OrderDeliverable', backref=db.backref('order_deliverable_cuts',
                                                                               cascade="all, delete-orphan",
                                                                               passive_deletes=True))

    cut_id = db.Column(db.Integer, db.ForeignKey('cuts.id'), nullable=False)
    cut = db.relationship('Cut', backref=db.backref('order_deliverable_cuts'))

    def __repr__(self):
        return "vulcan.OrderDeliverable: %d" % self.id


class Inventory(db.Model, DefaultMixin):
    __tablename__ = "inventory"

    class Meta:
        verbose_name = 'inventory entry'
        verbose_name_plural = 'inventory entries'
        ordering = ('ident',)

    ident = db.Column(db.String(45), nullable=False)
    x = db.Column(db.Integer, nullable=False)
    y = db.Column(db.Integer, nullable=False)
    z = db.Column(db.Integer, nullable=False)
    place = db.Column(db.String(45), nullable=False)
    received_at = db.Column(db.TIMESTAMP, onupdate=db.func.utc_timestamp())
    active = db.Column(db.Boolean, nullable=False, default='1')
    manual = db.Column(db.Boolean, nullable=False, default='0')

    cut_id = db.Column(db.Integer, db.ForeignKey('cuts.id', onupdate='SET NULL', ondelete='SET NULL'))
    cut = db.relationship('Cut', backref=db.backref('inventory'))

    quality_id = db.Column(db.Integer, db.ForeignKey('qualities.id'), nullable=False)
    quality = db.relationship('Quality', backref=db.backref('inventory'))

    def __repr__(self):
        return "vulcan.Inventory: %d" % self.id


def property_mixins():
    OrderDeliverable.total = column_property(
        func.coalesce(select([OrderDeliverable.amount - func.sum(OrderDeliverableCut.amount)]).
        where(OrderDeliverableCut.order_deliverable_id == OrderDeliverable.id).
        group_by(OrderDeliverable.id).as_scalar(), OrderDeliverable.amount)
    )

    Inventory.weight = column_property((Inventory.x * Inventory.y * Inventory.z)* WEIGHT_DM
                                       / 1000000.)
    OrderDeliverable.weight = column_property((OrderDeliverable.x * OrderDeliverable.y * OrderDeliverable.z)* WEIGHT_DM
                                              / 1000000.)
