import __builtin__

import abc
import io
import os
import shutil
import math
import mimetypes
from os import path

import boto3
from botocore import exceptions as botoex

from flask import current_app

clsmap = {}


def _get_handler_class():
    clsn = current_app.config['FILESYSTEM_CLASS']
    if clsn not in clsmap:
        clsmap[clsn] = globals().get(clsn)

    return clsmap[clsn]


def deletedir(dirname):
    return _get_handler_class()().deletedir(dirname)


def listdir(dirname):
    return _get_handler_class()().listdir(dirname)


def open(filename, mode='r'):
    return _get_handler_class()(filename, mode)


class VulcanFilesystem(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, filename=None, mode='r'):
        self.filename = filename
        self.mode = mode

    @abc.abstractmethod
    def __enter__(self):
        pass

    @abc.abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class VulcanFilesystemLocal(VulcanFilesystem):
    def __init__(self, filename=None, mode='r'):
        filename = self._process_dir(filename)

        super(VulcanFilesystemLocal, self).__init__(filename, mode)

    def __enter__(self):
        head, tail = path.split(self.filename)
        if not path.exists(head):
            os.mkdir(head)

        self.fd = __builtin__.open(self.filename, self.mode)
        return self.fd

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fd.close()

    @staticmethod
    def _process_dir(dirname):
        if dirname:
            return path.join(current_app.config['APP_ROOT'], dirname)

    def listdir(self, dirname):
        dirname = self._process_dir(dirname)
        return os.listdir(dirname)

    def deletedir(self, dirname):
        dirname = self._process_dir(dirname)
        return shutil.rmtree(dirname, ignore_errors=True)


class VulcanFilesystemS3(VulcanFilesystem):
    class InternalS3File(io.BufferedIOBase):
        def __init__(self, s3_obj, fname):
            self.obj = s3_obj
            self.filename = fname

        def get_url(self):
            try:
                obj = self.obj.generate_presigned_url('get_object',
                                                      Params={'Bucket': 'heroku-vulcan', 'Key': self.filename})
            except botoex.ClientError:
                raise IOError("File not found")

            return obj

        def read(self, n=None):
            if n < 0:
                n = None

            try:
                obj = self.obj.get_object(Bucket='heroku-vulcan', Key=self.filename)
            except botoex.ClientError:
                raise IOError("File not found")
            return obj['Body'].read(amt=n)

        def write(self, body):
            self.obj.put_object(
                ACL='public-read',
                Body=body,
                Bucket='heroku-vulcan',
                Key=self.filename,
                ContentType=VulcanFilesystemS3._find_content_type(self.filename)
            )

        def close(self):
            pass

    def __init__(self, filename=None, mode='r'):
        filename = self._process_dir(filename)

        super(VulcanFilesystemS3, self).__init__(filename, mode)

        self.s3 = boto3.client(
            's3',
            aws_access_key_id=current_app.config['AWS_ACCESS_KEY'],
            aws_secret_access_key=current_app.config['AWS_SECRET_ACCESS_KEY']
        )

        # Load WinReg defaults first, if applicable
        mimetypes.init()
        # Load the custom mimetypes file
        mimetypes.init(["mime.types"])

    def __enter__(self):
        self.fd = self.InternalS3File(self.s3, self.filename)

        return self.fd

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fd.close()

    @staticmethod
    def _process_dir(dirname):
        if dirname:
            return '/'.join(dirname.split('\\'))

    @staticmethod
    def _find_content_type(dirname):
        return mimetypes.guess_type(dirname)[0]

    def _listdir(self, dirname):
        dirname = self._process_dir(dirname)

        all_keys = []
        token = None
        truncated = True
        while truncated:
            if token:
                key_list = self.s3.list_objects_v2(
                    Bucket='heroku-vulcan',
                    ContinuationToken=token,
                    Prefix=dirname
                )
            else:
                key_list = self.s3.list_objects_v2(
                    Bucket='heroku-vulcan',
                    Prefix=dirname
                )

            all_keys += key_list['Contents']
            truncated = key_list['IsTruncated']
            if truncated:
                token = key_list['NextContinuationToken']

        return all_keys

    def listdir(self, dirname):
        base_keys = self._listdir(dirname)

        all_keys = []
        for key in base_keys:
            all_keys.append(key['Key'])

        return all_keys

    def deletedir(self, dirname):
        key_list = self._listdir(dirname)

        # 1000 key limit per S3 request
        pages = int(math.ceil(len(key_list) / 1000.0))
        for i in range(pages):
            start = i * 1000
            delete = []
            for key in key_list[start:start+1000]:
                delete.append({'Key': key['Key']})

            self.s3.delete_objects(
                Bucket='heroku-vulcan',
                Delete={'Objects': delete}
            )
