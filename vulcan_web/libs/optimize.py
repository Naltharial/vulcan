import jsonpickle
import bisect
import io
from collections import namedtuple

from sqlalchemy import and_

from flask import current_app
from flask_babel import gettext as _

from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Border, Side, PatternFill, NamedStyle
from openpyxl.utils import get_column_letter

from vulcan_web.extensions import db
from vulcan_web.models import vulcan
from vulcan_web.libs import visualize, WEIGHT_DM
from vulcan_web.libs.filesystem import open, path

CUT_ROTATIONS = (
    (0, 1, 2,),
    (0, 2, 1,),
    (1, 0, 2,),
    (1, 2, 0,),
    (2, 0, 1,),
    (2, 1, 0,),
)
VIS_PATH = 'libs/optimize'

# bool(status) == final_cut
ST_MID = 0
ST_EMPTY = 1
ST_RESULT = 2


def optimize(orders, **kwargs):
    try:
        settings = {
            'merge': float(kwargs.get('merge', 0.)),
            'cid': int(kwargs.get('cid', None)),
            'cut_loss': kwargs.get('cut_loss', None),
            'cut_minimum': kwargs.get('cut_minimum', None),
            'inventory_id': kwargs.get('inventory_id', None)
        }
    except ValueError:
        raise CutException(_("Please enter correct numeric values for settings"))

    if settings['cut_loss']:
        settings['cut_loss'] = float(settings['cut_loss'])
        if settings['cut_loss'] < 0.:
            raise CutException(_("Cut loss cannot be negative"))
    else:
        settings['cut_loss'] = None

    if settings['cut_minimum']:
        settings['cut_minimum'] = float(settings['cut_minimum'])
        if settings['cut_minimum'] <= 0.:
            raise CutException(_("Minimum cut size must be greater than zero"))
    else:
        settings['cut_minimum'] = None

    inventory = None
    if settings['inventory_id']:
        try:
            inventory_id = int(settings['inventory_id'])
            inv = vulcan.Inventory.query.get(inventory_id)
            if not inv:
                raise CutException(_("Incorrect selected inventory: '%s'") % settings['inventory_id'])
            else:
                inventory = [inv]
        except ValueError:
            pass

    okeys = orders.keys() if isinstance(orders, dict) else orders
    order_del_raw = vulcan.OrderDeliverable.query.filter(and_(
        vulcan.OrderDeliverable.order_id.in_(okeys),
        vulcan.OrderDeliverable.total > 0
    )).order_by(
        vulcan.OrderDeliverable.x +
        2 * vulcan.OrderDeliverable.y +
        vulcan.OrderDeliverable.z
    ).all()

    order_del = []
    if isinstance(orders, dict):
        for odel in order_del_raw:
            order = orders.get(str(odel.order.id), None)
            if order:
                amount = order.get(str(odel.id), None)
                if amount:
                    amount = int(amount)
                    if amount:
                        if amount > odel.amount:
                            raise CutException(_("Too many selected deliverables: '%s', selected %d of %d")
                                               % (odel.name, amount, odel.amount))
                        new_del = Element(odel.id, None, odel.x, odel.y, odel.z, odel.name, odel.quality, amount)
                        order_del.append(new_del)
    else:
        order_del = order_del_raw

    p_t = VIS_PATH.split('/') + ["%d" % settings['cid']]
    fpath = path.join(current_app.config['APP_STATIC_TOK'], *p_t)

    cut_man = CutManager(
        settings['cid'],
        fpath,
        cut_loss=settings['cut_loss'],
        cut_minimum=settings['cut_minimum']
    )
    cut_man.process_elements(
        order_del,
        inventory=inventory
    )

    cut_man.merge(settings['merge'] / 100.)

    cut_man.save()
    cut_man.export()


def save_sequence(cut_id, save_path, sequence, unmatched=None):
    data = {
        'unmatched': unmatched,
        'sequence': sequence
    }

    frozen = jsonpickle.encode(data)

    with open(path.join(save_path, "Process-%d.txt" % (cut_id,)), 'w') as fp:
        fp.write(frozen)


def export_sequence(cut_id, save_path, sequence):
    wb = Workbook()
    ws = wb.active

    bold_font = Font(b=True)
    bolditalic_font = Font(b=True, i=True)
    gray_back = PatternFill(patternType='lightGray')
    bold_border = Border(left=Side(style='medium'),
                         right=Side(style='medium'),
                         top=Side(style='medium'),
                         bottom=Side(style='medium'))
    right_align = Alignment(horizontal='right')
    border_style = NamedStyle(name='border', border=bold_border)
    bifont_style = NamedStyle(name='bifont', font=bolditalic_font)
    bborder_style = NamedStyle(name='bborder', border=bold_border, font=bold_font)
    fborder_style = NamedStyle(name='fborder', fill=gray_back, border=bold_border)

    for pid, process in enumerate(sequence):
        total_time = 0
        for step in process:
            if not step.status:
                total_time += step.total_time
        m, s = divmod(total_time, 60)
        h, m = divmod(m, 60)
        time_str = "%d:%02d:%02d" % (h, m, s)

        ws.append(["%d. IDENT" % (pid + 1), process[0].ident])
        begin_r = ws.max_row
        begin_c = get_column_letter(1)
        end_c = get_column_letter(6)
        ws.append([_("height"), _("width"), _("length"), _("quality"), _("weight"), _("place"), _("time")])
        ws.append([
            process[0].y,
            process[0].z,
            process[0].x,
            process[0].quality.name,
            str(round(process[0].weight, 2)) + " kg",
            process[0].place,
            time_str
        ])
        end_r = ws.max_row
        for row in ws['%s%d:%s%d' % (begin_c, begin_r + 1, end_c, end_r,)]:
            for cid, cell in enumerate(row):
                if cid < 5:
                    cell.style = fborder_style
                else:
                    cell.alignment = right_align
        ws.append([])

        ws.append([_("Results")])
        ws.append([_("name"), _("height"), _("width"), _("length"), _("weight"), _("amount")])
        begin_r = ws.max_row
        begin_c = get_column_letter(1)
        end_c = get_column_letter(6)
        total_weight = 0
        total_amount = 0
        named_places = {}
        for pl_emplace, pl_element in process[0].places:
            if pl_element.name not in named_places:
                named_places[pl_element.name] = [
                    pl_element.y,
                    pl_element.z,
                    pl_element.x,
                    str(round(pl_element.weight, 2)) + " kg",
                    0
                ]
            named_places[pl_element.name][4] += 1
            total_amount += 1
            total_weight += pl_element.weight
        for name, place in named_places.iteritems():
            ws.append([name] + place)
        end_r = ws.max_row
        for row in ws['%s%d:%s%d' % (begin_c, begin_r + 1, end_c, end_r,)]:
            for cell in row:
                cell.style = bifont_style
        ws.append([_("Total"), "", "", "", str(round(total_weight, 2)) + " kg", total_amount])
        ws.append([])

        ws.append([_("Process")])
        ws.append(["", _("height"), _("width"), _("length"), _("action"), _("location"), _("time")])
        begin_r = ws.max_row
        begin_c = get_column_letter(1)
        end_c = get_column_letter(7)
        bold = []
        for sid, step in enumerate(process):
            action = _("stock")
            location = ""
            if step.status == ST_RESULT:
                action = _("order")
                location = step.places[0][1].name
            elif step.status == ST_MID:
                action, location = _("height"), step.cut_at.y
                if step.cut_at.z:
                    action, location = _("width"), step.cut_at.z
                if step.cut_at.x:
                    action, location = _("length"), step.cut_at.x
            ln = ["%d" % (sid + 1), step.y, step.z, step.x, action, location]
            if not step.status:
                ln.append(step.time_str)
            ws.append(ln)
            if step.status == ST_RESULT:
                bold.append(sid + 1)
        end_r = ws.max_row
        for rid, row in enumerate(ws['%s%d:%s%d' % (begin_c, begin_r, end_c, end_r,)]):
            for cid, cell in enumerate(row):
                if cid > 5:
                    cell.alignment = right_align
                else:
                    if rid in bold:
                        cell.style = bborder_style
                    else:
                        cell.style = border_style
        ws.append([])

    tmp = io.BytesIO()
    wb.save(tmp)
    tmp.seek(0)

    with open(path.join(save_path, "Export-%d.xlsx" % (cut_id,)), 'wb') as fp:
        fp.write(tmp.read())

    tmp.close()


Emplace = namedtuple("Emplace", "x y z")


class Element(object):
    @property
    def volume(self):
        return self.x * self.y * self.z

    @property
    def weight(self):
        return self.volume * WEIGHT_DM / 1000000.

    def __init__(self, id=None, ref=None, x=None, y=None, z=None, name=None, quality=None, amount=None):
        self.id = id
        self.ref = ref
        self.x = x
        self.y = y
        self.z = z
        self.name = name
        self.quality = quality
        self.amount = amount


class CutManager(object):
    CUT_LOSS = 3.
    CUT_MINIMUM = 5.

    @property
    def sequence(self):
        if not self._sequence:
            self.process()

        return self._sequence

    @property
    def unmatched(self):
        if not self._unmatched:
            self.process()

        return self._unmatched

    def _size_match(self, cut_bin, element):
        eq = 0
        dist = 0
        remain = ['x', 'y', 'z']

        if cut_bin.x < element.x or cut_bin.y < element.y or cut_bin.z < element.z:
            return -1, None, None

        if (cut_bin.x - element.x) < self.CUT_MINIMUM:
            eq += 1
            dist += cut_bin.x - element.x
            remain.remove('x')
        if (cut_bin.y - element.y) < self.CUT_MINIMUM:
            eq += 1
            dist += cut_bin.y - element.y
            remain.remove('y')
        if (cut_bin.z - element.z) < self.CUT_MINIMUM:
            eq += 1
            dist += cut_bin.z - element.z
            remain.remove('z')

        remain_min = min(getattr(cut_bin, dim) - getattr(element, dim) for dim in remain)

        return eq, dist, remain_min

    def __init__(self, cid, path=None, cut_loss=None, cut_minimum=None):
        self._cid = int(cid)
        self._path = path

        self._sequence = None
        self._unmatched = None

        self.bins = {}
        self.elements = {}

        if cut_loss:
            self.CUT_LOSS = cut_loss
        if cut_minimum:
            self.CUT_MINIMUM = cut_minimum

    def process_elements(self, elements, inventory=None):
        for el in elements:
            if el.quality.name not in self.bins:
                self.bins[el.quality.name] = []

            if el.quality.name not in self.elements:
                self.elements[el.quality.name] = []
            self.elements[el.quality.name].append(CutElement(el))

        if not inventory:
            for ql in self.bins:
                inventory = vulcan.Inventory.query.filter(and_(
                    vulcan.Inventory.active.is_(True),
                    vulcan.Inventory.cut_id.is_(None),
                    vulcan.Quality.id == vulcan.Inventory.quality_id,
                    vulcan.Quality.name == ql
                )).all()

                for inv in inventory:
                    cut_bin = CutBin(inv, path=self._path, cut_loss=self.CUT_LOSS, cut_minimum=self.CUT_MINIMUM)
                    self.bins[ql].append(cut_bin)
        else:
            for inv in inventory:
                if inv.quality.name in self.bins:
                    cut_bin = CutBin(inv, path=self._path, cut_loss=self.CUT_LOSS, cut_minimum=self.CUT_MINIMUM)
                    self.bins[inv.quality.name].append(cut_bin)

    def match(self):
        unmatched = []
        for quality, cut_bins in self.bins.iteritems():
            cut_bins.sort(key=lambda x: x.volume)
            elements = self.elements[quality]

            for i in range(3, -1, -1):
                for cut_bin in cut_bins:
                    placed = 0
                    candidate = {}
                    for element in elements:
                        place = self._find_best_rotation(cut_bin, element, i)

                        if place:
                            for dist, rot in place.iteritems():
                                if dist not in candidate:
                                    candidate[dist] = []
                                candidate[dist].append((element, rot,))

                    if candidate:
                        low = candidate.keys()
                        low.sort()
                        for dist in low:
                            cut_elements = candidate[dist]
                            for cut_el, rot in cut_elements:
                                if not cut_el.amount:
                                        continue
                                cp = self._emplace(cut_bin, cut_el, placed, rotations=rot)
                                placed += cp

            for element in elements:
                if element.amount:
                    unmatched.append("%s (x%d)" % (element.name, element.amount,))

        return unmatched

    def merge(self, ratio):
        if ratio > 100 or ratio < 0:
            raise CutException(_("Combine value not a valid percentage"))

        for cut_bins in self.bins.values():
            filled_bins = []
            for cut_bin in cut_bins:
                if len(cut_bin.places) > 0:
                    filled_bins.append(cut_bin)

            filled_bins.sort(key=lambda x: x.filled)

            for idx, cut_bin in enumerate(filled_bins):
                if cut_bin.filled * 100 < ratio:
                    rem = []
                    for pl_idx, pl_el in cut_bin.places:
                        pl_emplace, pl_element = pl_el
                        cur_match = self._size_match(cut_bin, pl_element)
                        new_el = CutElement(
                            pl_element,
                            amount=1
                        )

                        # Push elements into better filled bins ahead
                        placed = False
                        for next_bin in filled_bins[idx+1:]:
                            new_match = self._size_match(next_bin, pl_element)
                            if new_match[0] < cur_match[0]:
                                continue
                            placed = self._find_best_rotation(next_bin, new_el)
                            if placed:
                                placed = self._emplace(cut_bin, new_el, reference=pl_element.ref)
                                if placed:
                                    break
                        if placed:
                            rem.append(idx)

                    for el in rem.reverse():
                        del cut_bin.places[el]
                    if rem:
                        cut_bin.reorder()

    def get_sequence(self):
        result = []
        bins = []
        for cut_bins in self.bins.values():
            for cut_bin in cut_bins:
                if len(cut_bin.places) > 0:
                    bins.append(cut_bin)

        for cut_bin in bins:
            sequence = cut_bin.cut()
            for element in sequence:
                element.visualize()
            result.append(sequence)

        return result

    def _find_best_rotation(self, cut_bin, element, match=0):
        rotations = {}
        remains = {}
        for rotation in range(len(CUT_ROTATIONS)):
            element.rotate(rotation)
            eq, dist, remain = self._size_match(cut_bin, element)
            if eq >= match:
                if dist not in rotations:
                    rotations[dist] = []
                    remains[dist] = []
                place = bisect.bisect_left(remains[dist], remain)
                rotations[dist].insert(place, rotation)
                remains[dist].insert(place, remain)

        return rotations

    def _emplace(self, cut_bin, element, placed=0, reference=None, rotations=None):
        rotations = rotations or CUT_ROTATIONS
        for rotation in rotations:
            element.rotate(rotation)
            while element.amount > 0:
                if reference is None:
                    el_ref = "%d:%d" % (element.id, placed,)
                else:
                    el_ref = reference

                result = cut_bin.emplace(element, el_ref, cut_id=self._cid)
                if not result:
                    break
                else:
                    placed += 1
                    element.amount -= 1

        return placed

    def process(self):
        self._unmatched = self.match()
        self._sequence = self.get_sequence()

    def save(self):
        save_sequence(self._cid, self._path, self.sequence, self.unmatched)

    def export(self):
        export_sequence(self._cid, self._path, self.sequence)

    def __repr__(self):
        return "CutManager: %s" % [("%s: %d" % (ql, len(bins),)) for ql, bins in self.bins.iteritems()]


class CutBin(object):
    """
    Hold cut-box information
    """
    CUT_LOSS = 3.
    CUT_MINIMUM = 5.

    @property
    def time_str(self):
        m, s = divmod(self.total_time, 60)
        h, m = divmod(m, 60)
        return "%d:%02d:%02d" % (h, m, s)

    @property
    def reference(self):
        return "%d-%s-%s" % (self.id, self.ident, self._level)

    @property
    def volume(self):
        return self.x * self.y * self.z

    @property
    def weight(self):
        return self.volume * WEIGHT_DM / 1000000.

    @property
    def filled(self):
        if not self.places:
            return 0.

        vol_elem = 0.
        for pl_emplace, pl_element in self.places:
            vol_elem += pl_element.x * pl_element.y * pl_element.z

        return vol_elem / self.volume

    def __init__(self, inventory, level="b", path=None, cut_loss=None, cut_minimum=None):
        self._inventory = inventory
        self._cuts = None
        self._path = path
        self._level = level

        self.places = []

        self.cut_at = None
        self.status = ST_MID
        self.total_time = 1

        if cut_loss:
            self.CUT_LOSS = cut_loss
        if cut_minimum:
            self.CUT_MINIMUM = cut_minimum

    def cut(self):
        sequence = [self]
        if not self.places:
            self.status = ST_EMPTY
            return sequence

        split = self._find_best_cut()
        if not split and len(self.places) == 1:
            self.status = ST_RESULT
            return sequence

        lr = self.split(split)
        if lr:
            left, right = lr
            left_cut = left.cut()
            if left_cut:
                sequence += left_cut
            right_cut = right.cut()
            if right_cut:
                sequence += right_cut

        return sequence

    def emplace(self, element, reference, cut_id=None):
        search_x = self.x - element.x
        search_y = self.y - element.y
        search_z = self.z - element.z

        if search_x < 0 or search_y < 0 or search_z < 0:
            return None

        dims = {'x': search_x, 'y': search_y, 'z': search_z}
        dim_keys = sorted(['x', 'y', 'z'], key=lambda k: dims[k], reverse=True)

        def _check(x, y, z):
            new_el = (
                Emplace(x, y, z),
                Element(
                    element.id,
                    reference,
                    element.x,
                    element.y,
                    element.z,
                    element.name
                ),
            )

            if self.places and not self._find_best_cut(new_el):
                return None

            return new_el

        def _detect(x, y, z, scan='z'):
            for pl_emplace, pl_element in self.places:
                if x + element.x + self.CUT_LOSS > pl_emplace.x and \
                                x < pl_emplace.x + pl_element.x + self.CUT_LOSS:
                    if y + element.y + self.CUT_LOSS > pl_emplace.y and \
                                    y < pl_emplace.y + pl_element.y + self.CUT_LOSS:
                        if z + element.z + self.CUT_LOSS > pl_emplace.z and \
                                        z < pl_emplace.z + pl_element.z + self.CUT_LOSS:
                            # Hits existing element
                            return getattr(pl_emplace, scan) + getattr(pl_element, scan) + self.CUT_LOSS

        def _search(x, y, z, scan='z'):
            new_z = _detect(x, y, z, scan=scan)

            if not new_z:
                # No obstacles found
                new_el = _check(x, y, z)
                if new_el:
                    self.places.append(new_el)
                    self._cuts = None
                    return -1
                else:
                    return z + 1
            else:
                return new_z

        for d0 in range(dims[dim_keys[0]]+1):
            for d1 in range(dims[dim_keys[1]]+1):
                d2 = 0
                while d2 <= dims[dim_keys[2]]:
                    cur_dims = {dim_keys[0]: d0, dim_keys[1]: d1, dim_keys[2]: d2}
                    find = _search(scan=dim_keys[2], **cur_dims)

                    if find < 0:
                        if cut_id:
                            self._inventory.cut_id = cut_id
                            db.session.add(self._inventory)

                            filters = {
                                'cut_id': cut_id,
                                'order_deliverable_id': element.id
                            }
                            odc, created = vulcan.OrderDeliverableCut.get_or_create({'amount': 1}, **filters)
                            if not created:
                                odc.amount += 1
                            db.session.add(odc)

                        return True
                    else:
                        d2 = find

    def visualize(self):
        if self._path is not None:
            filename = path.join(self._path, "%s.svg" % self.reference)
            with open(filename, 'w') as fp:
                visualize.cuboid(self, fp, loss=self.CUT_LOSS)

    def split(self, emplace=None):
        dims = ('x', 'y', 'z',)
        if emplace is None:
            emplace = self._find_best_cut()
        if emplace is None:
            return
        self.cut_at = emplace
        self.total_time = self._get_cut_area(emplace) / (self.quality.cut_time * 100. / 60.)

        left = CutBin(
            vulcan.Inventory(
                id=self.id,
                ident=self.ident,
                x=self.x,
                y=self.y,
                z=self.z,
                place=self.place,
                quality=self.quality
            ),
            self._level + "l",
            self._path,
            cut_loss=self.CUT_LOSS,
            cut_minimum=self.CUT_MINIMUM
        )
        right = CutBin(
            vulcan.Inventory(
                id=self.id,
                ident=self.ident,
                x=self.x,
                y=self.y,
                z=self.z,
                place=self.place,
                quality=self.quality
            ),
            self._level + "r",
            self._path,
            cut_loss=self.CUT_LOSS,
            cut_minimum=self.CUT_MINIMUM
        )
        # Do not persist fake inventory back to database
        db.session.expunge(left._inventory)
        db.session.expunge(right._inventory)

        for idx, dim in enumerate(dims):
            cut_dim = getattr(emplace, dim)
            if cut_dim:
                setattr(left._inventory, dim, cut_dim)
                full_dim = getattr(right._inventory, dim)
                setattr(right._inventory, dim, full_dim - cut_dim - self.CUT_LOSS)

                for pl_emplace, pl_element in self.places:
                    if getattr(pl_emplace, dim) < cut_dim:
                        left.places.append((pl_emplace, pl_element))
                    else:
                        new_emplace = [pl_emplace.x, pl_emplace.y, pl_emplace.z]
                        cur_emp = getattr(pl_emplace, dim)
                        new_emplace[idx] = cur_emp - cut_dim - self.CUT_LOSS
                        new_emplace = Emplace(*new_emplace)
                        right.places.append((new_emplace, pl_element))
        return left, right,

    def reorder(self):
        elements = [y for x, y in self.places]
        self.places = []
        if elements:
            for element in elements:
                new_el = CutElement(
                    element,
                    amount=1
                )
                placed = self._emplace(self, new_el, element.ref)
                if not placed:
                    raise CutException("Bin cannot contain previously contained elements")
        else:
            self._inventory.cut_id = None
            db.session.add(self._inventory)

    def _find_best_cut(self, additional=None):
        candidates = []
        places = list(self.places)
        if additional:
            places.append(additional)
        for pl_emplace, pl_element in places:
            candidates.append(Emplace(pl_emplace.x + pl_element.x, 0, 0))
            candidates.append(Emplace(0, pl_emplace.y + pl_element.y, 0))
            candidates.append(Emplace(0, 0, pl_emplace.z + pl_element.z))

        if not candidates:
            # No places, basic split check
            emp = Emplace(int(self.x/2), 0, 0)
            if self._check_cut(emp):
                return emp
            else:
                return

        def sorter(place):
            return self._get_cut_area(place)

        candidates.sort(key=sorter)

        for candidate in candidates:
            if self._check_cut(candidate):
                return candidate

        if len(self.places) > 1:
            raise CutException("Full container cannot be cut.")

    def _check_cut(self, candidate):
        if not candidate.x and not candidate.y and not candidate.z:
            return False
        dims = ('x', 'y', 'z',)
        for dim in dims:
            cd = getattr(candidate, dim)
            if cd:
                if cd < self.CUT_MINIMUM or getattr(self, dim) - cd - self.CUT_LOSS < self.CUT_MINIMUM:
                    return False

        for pl_emplace, pl_element in self.places:
            if candidate.x:
                if candidate.x + self.CUT_LOSS > pl_emplace.x and \
                        candidate.x < pl_emplace.x + pl_element.x:
                    return False
            if candidate.y:
                if candidate.y + self.CUT_LOSS > pl_emplace.y and \
                        candidate.y < pl_emplace.y + pl_element.y:
                    return False
            if candidate.z:
                if candidate.z + self.CUT_LOSS > pl_emplace.z and \
                        candidate.z < pl_emplace.z + pl_element.z:
                    return False

        return True

    def _get_cut_area(self, cut):
        dims = ('x', 'y', 'z',)
        key = 'x'
        if cut.y:
            key = 'y'
        elif cut.z:
            key = 'z'
        dims = tuple((el for el in dims if el != key))
        area = getattr(self, dims[0]) * getattr(self, dims[1])

        return area

    def __getattr__(self, attr):
        if '_inventory' in self.__dict__:
            return getattr(self._inventory, attr)
        else:
            raise AttributeError("%r object has no attribute %r" %
                                 (self.__class__, attr))

    def __getstate__(self):
        state = self.__dict__.copy()
        state['_inventory'] = {
                'id': self._inventory.id,
                'ident': self._inventory.ident,
                'x': self._inventory.x,
                'y': self._inventory.y,
                'z': self._inventory.z,
                'place': self._inventory.place,
                'quality': self._inventory.quality.id
        }
        return state

    def __setstate__(self, state):
        quality = vulcan.Quality.query.get(state['_inventory']['quality'])
        ninv = vulcan.Inventory(
            id=state['_inventory']['id'],
            ident=state['_inventory']['ident'],
            x=state['_inventory']['x'],
            y=state['_inventory']['y'],
            z=state['_inventory']['z'],
            place=state['_inventory']['place'],
            quality=quality
        )
        db.session.expunge(ninv)
        del state['_inventory']
        self.__dict__.update(state)
        self._inventory = ninv

    def __repr__(self):
        return "CutBin %s: %dx%dx%d elements(%d) full(%d%%)" % (
            self.reference, self.x, self.y, self.z, len(self.places), int(self.filled * 100)
        )


class CutElement(object):
    """
    Wrap vulcan.Element with a rotating element
    """
    @property
    def x(self):
        return getattr(self._element, self._rotated[0])

    @property
    def y(self):
        return getattr(self._element, self._rotated[1])

    @property
    def z(self):
        return getattr(self._element, self._rotated[2])

    def __init__(self, element, rotation=0, amount=None):
        self.rotation = rotation
        if amount is None:
            self.amount = element.amount
        else:
            self.amount = amount

        self._element = element
        self._rotated = ('x', 'y', 'z',)

    def rotate(self, rotation):
        names = ('x', 'y', 'z',)
        cur = CUT_ROTATIONS[rotation]
        self.rotation = rotation
        self._rotated = tuple((names[d] for d in cur))

    def reset(self):
        self.rotation = 0
        self._rotated = ('x', 'y', 'z',)

    def __getattr__(self, name):
        return getattr(self._element, name)

    def __repr__(self):
        return "CutElement %s: %dx%dx%d (%d)" % (self.id, self.x, self.y, self.z, self.amount,)


class CutException(Exception):
    pass
