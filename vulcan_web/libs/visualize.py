import math

from PyImage3D import Image3D, POLY_WIRE_BOTH
from PyImage3D.Image import color
from PyImage3D.Image.Renderer import perspective
from PyImage3D.Image.Driver import SVG
from PyImage3D.Image.Matrix import move, rotate, scale
from PyImage3D.Image.Paintable.Light import ambient
from PyImage3D.Image.Paintable.Object import zcube

IMAGE_SIZE = 500
OBJECT_SCALE = 1.4

# Keep the same elements represented with the same color
_color_cache = {}


def cuboid(cut_bin, filename, image_size=IMAGE_SIZE, object_scale=OBJECT_SCALE, loss=None):
    i3d = Image3D.Image3D()
    i3d.set_color(color.ImageColor(240, 240, 240))

    l = i3d.create_light(ambient.ImagePaintableLightAmbient, [-300, 0, -300])
    l.set_color(color.ImageColor(255, 255, 255))

    def rgb(r, g, b):
        return color.ImageColor(r, g, b)

    colors = [
        rgb(170, 0, 255), rgb(178, 167, 0), rgb(95, 230, 46), rgb(73, 28, 140), rgb(204, 98, 82), rgb(102, 41, 57),
        rgb(229, 122, 0), rgb(102, 68, 0), rgb(191, 38, 140), rgb(92, 220, 230), rgb(41, 102, 49), rgb(130, 130, 217)
    ]
    red = rgb(255, 0, 0)
    black = rgb(0, 0, 0)
    el = i3d.create_object(zcube.ImagePaintableObjectZCube, (cut_bin.x, cut_bin.y, cut_bin.z,))
    el.wireframe = True
    el.set_color(black)

    i = 0
    for emplace, element in cut_bin.places:
        m_mat = i3d.create_matrix(move.ImageMatrixMove, (emplace.x, emplace.y, emplace.z,))

        cube = i3d.create_object(zcube.ImagePaintableObjectZCube, (element.x, element.y, element.z,))
        if element.ref in _color_cache:
            cube.set_color(_color_cache[element.ref])
        else:
            cc = colors[i % len(colors)]
            _color_cache[element.ref] = cc
            cube.set_color(cc)
        cube.transform(m_mat)
        cube.wireframe = POLY_WIRE_BOTH

        i += 1

    if cut_bin.cut_at:
        dims = ('x', 'y', 'z')
        wire_dim = [cut_bin.x, cut_bin.y, cut_bin.z]
        cut_dim = [cut_bin.cut_at.x, cut_bin.cut_at.y, cut_bin.cut_at.z]

        w_mat = i3d.create_matrix(move.ImageMatrixMove, cut_dim)

        for idx, dim in enumerate(dims):
            cd = getattr(cut_bin.cut_at, dim, 0)
            if cd:
                wire_dim[idx] = loss
                cw = i3d.create_object(zcube.ImagePaintableObjectZCube, wire_dim)
                cw.wireframe = True
                cw.set_color(red)
                cw.transform(w_mat)

    m_mat = i3d.create_matrix(move.ImageMatrixMove, (-cut_bin.x/2, -cut_bin.y/2, -cut_bin.z/2,))
    i3d.transform(m_mat)

    r_mat = i3d.create_matrix(rotate.ImageMatrixRotate, (210, 30, 10,))
    i3d.transform(r_mat)

    m_scale = min(
        image_size / (cut_bin.x * object_scale),
        image_size / (cut_bin.y * object_scale),
        image_size / (cut_bin.z * object_scale)
    ) * (object_scale + 1.0) / 2
    s_mat = i3d.create_matrix(scale.ImageMatrixScale, (m_scale, m_scale, m_scale,))
    i3d.transform(s_mat)

    i3d.create_renderer(perspective.ImageRendererPerspective)
    i3d.create_driver(SVG.ImageDriverSVG)

    i3d.render(image_size, image_size, filename)
