import os
import rollbar
import rollbar.contrib.flask
from flask import g, got_request_exception, Request

from vulcan_web.application import create_app, get_config

app = create_app(get_config('vulcan_web.config.Heroku'))


@app.before_first_request
def init_rollbar():
    """init rollbar module"""
    rollbar.init(
        '30b971e2a93548a6ba41ce41d5651475',
        'production',
        root=os.path.dirname(os.path.realpath(__file__)),
        allow_logging_basic_config=False)

    got_request_exception.connect(rollbar.contrib.flask.report_exception, app)


class CustomRequest(Request):
    @property
    def rollbar_person(self):
        return {'id': g.user.id, 'email': g.user.email}

app.request_class = CustomRequest

if __name__ == "__main__":
    app.run()
