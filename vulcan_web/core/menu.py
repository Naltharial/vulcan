# TODO: Generate from vulcan_web.blueprints
from flask import g
from collections import OrderedDict


def get_menu():
    menu = {
        'Inventory': OrderedDict(),
        'Cuts': OrderedDict({
            'List': ("cuts.list.index", "icn_categories"),
        }),
        'Orders': OrderedDict(),
        'Qualities': OrderedDict({
            'List': ("qualities.list.index", "icn_categories"),
        }),
    }

    menu['Inventory']['List'] = ("inventory.list.index", "icn_categories")
    menu['Inventory']['Import/Export'] = ("inventory.iex.index", "icn_folder")
    menu['Inventory']['Clear'] = ("inventory.clear.index", "icn_photo")

    menu['Orders']['New'] = ("orders.new.index", "icn_new_article")
    menu['Orders']['List'] = ("orders.list.index", "icn_categories")
    menu['Orders']['Optimization'] = ("orders.optimize.index", "icn_settings")
    menu['Orders']['Import/Export'] = ("orders.iex.index", "icn_folder")

    ordering = ['Inventory', 'Orders', 'Cuts', 'Qualities']

    if g.user.has_role('Admin'):
        menu['Qualities']['New'] = ("qualities.new.index", "icn_new_article")

        users = OrderedDict({
            'List': ("users.list.index", "icn_view_users"),
            'New': ("users.new.index", "icn_add_user"),
            'Roles': ("users.roles.index", "icn_security"),
        })

        menu['Users'] = users
        ordering.append('Users')

    return menu, ordering
