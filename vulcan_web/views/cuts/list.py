import datetime
import jsonpickle
from math import ceil

from sqlalchemy import and_

from flask import current_app, redirect, request, render_template, flash, url_for, send_from_directory
from flask_security import login_required
from flask_babel import gettext as _

from vulcan_web.blueprints import cuts_list
from vulcan_web.models import vulcan
from vulcan_web.extensions import db
from vulcan_web.libs.optimize import VIS_PATH, save_sequence
from vulcan_web.libs.filesystem import open, path, listdir, deletedir


@cuts_list.route('/')
@login_required
def index():
    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    query = vulcan.Cut.query.order_by(vulcan.Cut.cut_at.desc())
    results = query.limit(limit).offset(offset).all()
    pages = ceil(float(query.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page
    }
    return render_template('cuts_list.html', **data)


@cuts_list.route('/<int:cut_id>/')
@login_required
def detail(cut_id):
    result = vulcan.Cut.query.get(cut_id)
    if not result:
        flash(_(u"Incorrect URL for cut: %s") % cut_id, 'warning')
        return redirect(url_for('cuts.list.index'))

    path_tok = VIS_PATH.split('/') + ["%d" % cut_id]
    fpath = path.join(current_app.config['APP_STATIC_TOK'], *path_tok)
    basepath = path.join(current_app.config['APP_STATIC'], *path_tok)

    try:
        with open(path.join(fpath, "Process-%d.txt" % (cut_id,))) as fp:
            thawed = jsonpickle.decode(fp.read())
    except IOError:
        flash(_(u"Details for this cut are no longer available"), 'warning')
        thawed = {u'unmatched': [], u'sequence': []}

    unmatched = thawed['unmatched'] or []
    sequence = thawed['sequence'] or []

    totals = {
        'elements': {
            'weight': sum(order_deliverable_cut.order_deliverable.weight for order_deliverable_cut in result.order_deliverable_cuts),
            'amount': sum(order_deliverable_cut.amount for order_deliverable_cut in result.order_deliverable_cuts)
        },
        'items': {
            'weight': sum(result.weight for result in result.inventory),
            'amount': sum(1 for result in result.inventory)
        }
    }

    for miss in unmatched:
        flash(_(u"Inventory not found for: %s") % miss, 'warning')

    return render_template('cuts_optimize.html', cut=result, results=sequence, totals=totals, baseurl=basepath)


@cuts_list.route('/<int:cut_id>/approve/', methods=['POST'])
@login_required
def approve(cut_id):
    result = vulcan.Cut.query.get(cut_id)

    if result and result.status.name == "Pending":
        approved, created = vulcan.Status.get_or_create(name="Approved")

        path_tok = VIS_PATH.split('/') + ["%d" % cut_id]
        fpath = path.join(current_app.config['APP_STATIC_TOK'], *path_tok)

        with open(path.join(fpath, "Process-%d.txt" % (cut_id,))) as fp:
            thawed = jsonpickle.decode(fp.read())

        sequence = thawed['sequence']

        result.status = approved
        db.session.add(result)

        nels = []
        for cut_bins in sequence:
            for cut_bin in cut_bins:
                if len(cut_bin.places) > 0:
                    inv = vulcan.Inventory.query.get(cut_bin.id)
                    inv.active = False
                    db.session.add(inv)
                if cut_bin.status == 1:
                    ninv = vulcan.Inventory(
                        ident=cut_bin.ident,
                        x=cut_bin.x,
                        y=cut_bin.y,
                        z=cut_bin.z,
                        place=cut_bin.place,
                        quality=cut_bin.quality,
                        manual=0,
                        received_at=datetime.datetime.now()
                    )
                    db.session.add(ninv)
                    nels.append(ninv)

        db.session.commit()

    return redirect(url_for('cuts.list.index'))


@cuts_list.route('/<int:cut_id>/reject/', methods=['POST'])
@login_required
def reject(cut_id):
    result = vulcan.Cut.query.get(cut_id)

    if result and result.status.name == "Pending":
        path_tok = VIS_PATH.split('/') + ["%d" % cut_id]
        fpath = path.join(current_app.config['APP_STATIC_TOK'], *path_tok)

        for inv in result.inventory:
            # Setting _id delays the update until the entire set is traversed
            inv.cut_id = None

        rejected, created = vulcan.Status.get_or_create(name="Rejected")
        result.status = rejected
        db.session.add(result)

        vulcan.OrderDeliverableCut.query.filter(
            vulcan.OrderDeliverableCut.cut == result
        ).delete()

        db.session.commit()

        deletedir(fpath)

    return redirect(url_for('cuts.list.index'))


@cuts_list.route('/<int:cut_id>/detach/', methods=['POST'])
@login_required
def detach(cut_id):
    result = vulcan.Cut.query.get(cut_id)
    element_id = int(request.form['element_id'])
    inventory = vulcan.Inventory.query.get(element_id)

    if result.status.name == "Pending":
        inventory.cut_id = None
        db.session.add(inventory)

        path_tok = VIS_PATH.split('/') + ["%d" % cut_id]
        fpath = path.join(current_app.config['APP_STATIC_TOK'], *path_tok)

        with open(path.join(fpath, "Process-%d.txt" % (cut_id,))) as fp:
            thawed = jsonpickle.decode(fp.read())

        sequence = thawed['sequence']
        rm_pos = 0
        for cut_pos, cut_seq in enumerate(sequence):
            if cut_seq[0].id == element_id:
                rm_pos = cut_pos
                nums = {}
                for place, element in cut_seq[0].places:
                    if element.id not in nums:
                        nums[element.id] = 0

                    nums[element.id] += 1

                for od, num in nums.iteritems():
                    res = vulcan.OrderDeliverableCut.query.filter(and_(
                        vulcan.OrderDeliverableCut.cut_id == result.id,
                        vulcan.OrderDeliverableCut.order_deliverable_id == od
                    )).one()

                    if res.amount > num:
                        res.amount -= num
                        db.session.add(res)
                    else:
                        db.session.delete(res)

        del sequence[rm_pos]

        db.session.commit()

        save_sequence(cut_id, fpath, sequence, thawed['unmatched'])

    return redirect(url_for('cuts.list.detail', cut_id=cut_id))


@cuts_list.route('/<int:cut_id>/ex/')
@login_required
def ex(cut_id):
    p_t = VIS_PATH.split('/') + ["%d" % cut_id]
    fpath = path.join(current_app.config['APP_STATIC_TOK'], *p_t)

    for filename in listdir(fpath):
        if filename.endswith(".xlsx"):
            filename = path.basename(filename)

            url = '/'.join([current_app.config['APP_STATIC_ROOT'],
                            current_app.config['APP_STATIC_TOK'],
                            VIS_PATH,
                            str(cut_id),
                            filename])

            return redirect(url)

    return render_template('400.html'), 404
