import datetime

from flask import redirect, flash, request, render_template, url_for
from flask_security import login_required
from flask_babel import gettext as _

from vulcan_web.extensions import db
from vulcan_web.blueprints import orders_new
from vulcan_web.models import helpers, vulcan


@orders_new.route('/', methods=['GET','POST'])
@login_required
def index():
    data = {
        'order_number': '',
        'description': '',
        'posted': {},
        'qualities': vulcan.Quality.query.all()
    }
    if request.method == "POST":
        data['order_number'] = request.form['order_number']
        data['description'] = request.form['description']
        data['posted'] = helpers.deep_parse_post('parts', request.form)
        data['posted'] = {n: e for n, e in data['posted'].iteritems() if
                          e['name'] or e['amount']}

        check = True
        if len(data['posted']) < 1:
            flash(_(u"No parts added"), 'danger')
            check = False

        if not data['order_number']:
            flash(_(u"Missing order number"), 'danger')
            check = False
        else:
            try:
                int(data['order_number'])
            except ValueError:
                flash(_(u"Order number must be an integer"), 'danger')
                check = False

        if not data['description']:
            flash(_(u"Missing description"), 'danger')
            check = False

        for i in data['posted'].keys():
            i = int(i)
            if not data['posted'][str(i)]['name']:
                flash(_(u"Part %i: Missing name") % (i+1), 'danger')
                check = False

            if not data['posted'][str(i)]['y']:
                flash(_(u"Part %i: Missing height") % (i+1), 'danger')
                check = False
            else:
                try:
                    int(data['posted'][str(i)]['y'])
                except ValueError:
                    flash(_(u"Part %i: Height must be an integer") % (i+1), 'danger')
                    check = False

            if not data['posted'][str(i)]['z']:
                flash(_(u"Part %i: Missing width") % (i+1), 'danger')
                check = False
            else:
                try:
                    int(data['posted'][str(i)]['z'])
                except ValueError:
                    flash(_(u"Part %i: Width must be an integer") % (i+1), 'danger')
                    check = False

            if not data['posted'][str(i)]['x']:
                flash(_(u"Part %i: Missing length") % (i+1), 'danger')
                check = False
            else:
                try:
                    int(data['posted'][str(i)]['x'])
                except ValueError:
                    flash(_(u"Part %i: Length must be an integer") % (i+1), 'danger')
                    check = False

            if not data['posted'][str(i)]['amount']:
                flash(_(u"Part %i: Missing amount") % (i+1), 'danger')
                check = False
            else:
                try:
                    int(data['posted'][str(i)]['amount'])
                except ValueError:
                    flash(_(u"Part %i: Amount must be an integer") % (i+1), 'danger')
                    check = False

        if check:
            order = vulcan.Order(
                order_number=data['order_number'],
                description=data['description'],
                entered_at=datetime.datetime.now()
            )

            db.session.add(order)
            db.session.commit()

            for i, entry in data['posted'].iteritems():
                ql = vulcan.Quality.query.get(int(entry['quality']))
                en = vulcan.OrderDeliverable(
                    y=entry['y'],
                    z=entry['z'],
                    x=entry['x'],
                    name=entry['name'],
                    amount=entry['amount'],

                    order=order,
                    quality=ql
                )

                db.session.add(en)
                db.session.commit()

            return redirect(url_for('orders.list.index'))

    return render_template('orders_new.html', **data)
