from math import ceil

from flask import request, render_template
from flask_security import login_required

from vulcan_web.blueprints import orders_list
from vulcan_web.models import vulcan


@orders_list.route('/')
@login_required
def index():
    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    results = vulcan.Order.query.limit(limit).offset(offset).all()
    pages = ceil(float(vulcan.Order.query.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page
    }

    return render_template('orders_list.html', **data)


@orders_list.route('/<int:order_id>/')
@login_required
def detail(order_id):
    result = vulcan.Order.query.get(order_id)
    return render_template('orders_detail.html', result=result)
