import datetime
from io import BytesIO
from StringIO import StringIO
from openpyxl import load_workbook

from flask import flash, get_flashed_messages, redirect, request, render_template, url_for, send_file
from flask_babel import gettext as _
from flask_security import login_required
from werkzeug.utils import secure_filename

from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Border, Side, PatternFill, NamedStyle
from openpyxl.utils import get_column_letter

from vulcan_web.extensions import db
from vulcan_web.blueprints import orders_iex
from vulcan_web.models import vulcan


@orders_iex.route('/')
@login_required
def index():
    flash(get_flashed_messages())
    return render_template('orders_iex.html')


@orders_iex.route('/import/', methods=['POST'])
@login_required
def iexi():
    ifile = request.files['import']
    if not ifile.filename:
        flash(_('Please select a file.'), 'danger')
        return redirect(url_for('inventory.iex.index'))

    wb = load_workbook(filename=BytesIO(ifile.read()), read_only=True)

    for sheet in wb:
        shc = 0
        start = 3
        first = True
        entry = None

        for row in sheet.rows:
            if shc and shc < start:
                shc += 1
                continue

            if first:
                first = False
                if len(row) < 2:
                    break

                try:
                    if not row[3].value or int(row[3].value):
                        entry = vulcan.Order.query.filter_by(order_number=row[3].value).first()
                        if entry is None:
                            entry = vulcan.Order(
                                order_number=row[3].value,
                                description=row[1].value,
                                entered_at=datetime.datetime.now(),
                            )
                            if not entry.order_number:
                                entry.order_number = datetime.datetime.now().strftime("%y%m%d%H%M")
                except ValueError:
                    pass

                if entry is None:
                    entry = vulcan.Order(
                        order_number=datetime.datetime.now().strftime("%y%m%d%H%M"),
                        description=secure_filename(ifile.filename),
                        entered_at=datetime.datetime.now(),
                    )
                    start = 1

                try:
                    db.session.add(entry)
                    db.session.commit()
                except Exception as e:
                    flash(_("Error reading order header: '%s'") % e.message, 'warning')
                    break

                shc += 1
                continue

            if not row[0].value or not row[1].value or not row[2].value or not row[3].value or not row[5].value:
                flash(_(u"Order entry with name '%s' error - missing attributes") %
                                         (row[0].value,), 'error')
                continue

            quality = vulcan.Quality.query.filter_by(name=row[4].value).first()

            elem = vulcan.OrderDeliverable()
            elem.name = row[0].value
            elem.y = row[1].value
            elem.z = row[2].value
            elem.x = row[3].value
            elem.amount = row[5].value

            elem.order = entry

            if quality:
                elem.quality = quality
                db.session.add(elem)
            else:
                if len(row) > 7:
                    flash(_(u"Order entry with name '%s' error - quality '%s' does not exist") %
                                             (row[0].value, row[7].value), 'warning')
                else:
                    flash(_(u"Order entry with name '%s' error - quality does not exist") %
                                             row[0].value, 'warning')

    db.session.commit()

    return redirect(url_for('orders.list.index'))


@orders_iex.route('/export/', methods=['POST'])
@login_required
def iexe():
    wb = Workbook()
    wb.remove(wb.active)

    bold_font = Font(b=True)
    gray_back = PatternFill(patternType='lightGray')
    bold_border = Border(left=Side(style='medium'),
                         right=Side(style='medium'),
                         top=Side(style='medium'),
                         bottom=Side(style='medium'))
    bborder_style = NamedStyle(name='bborder', border=bold_border, font=bold_font)
    fborder_style = NamedStyle(name='fborder', fill=gray_back, border=bold_border)

    orders = vulcan.Order.query.all()

    for oid, order in enumerate(orders):
        title = order.description
        if not title:
            number = order.order_number
            if not number:
                number = order.id
            title = "%s %d" % (_("Order"), number)

        ws = wb.create_sheet(title=title, index=oid)

        begin_r = ws.max_row
        begin_c = get_column_letter(1)
        end_c = get_column_letter(7)
        ws.append([
            _("Name"),
            order.description,
            _("Order number"),
            order.order_number,
            "",
            "",
            "",
            "VID",
            order.id
        ])
        end_r = ws.max_row
        range_c = '%s%d:%s%d' % (begin_c, begin_r, end_c, end_r,)
        for row in ws[range_c]:
            for cell in row:
                cell.style = fborder_style

        ws.append([_("Results")])

        begin_r = ws.max_row + 1
        begin_c = get_column_letter(1)
        end_c = get_column_letter(6)
        ws.append([_("name"), _("height"), _("width"), _("length"), _("quality"), _("amount")])
        end_r = ws.max_row
        range_c = '%s%d:%s%d' % (begin_c, begin_r, end_c, end_r,)
        for row in ws[range_c]:
            for cell in row:
                cell.style = bborder_style

        for element in order.order_deliverables:
            ws.append([
                element.name,
                element.y,
                element.z,
                element.x,
                element.quality.name,
                element.amount
            ])

    out = StringIO()
    wb.save(out)
    out.seek(0)

    filename = u'%s-%s.xlsx' % (_(u"Orders"), datetime.date.today().isoformat())

    return send_file(out, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            attachment_filename=filename, as_attachment=True)
