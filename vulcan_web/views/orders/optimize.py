import os
import datetime

from sqlalchemy import and_

from flask import flash, request, redirect, url_for, render_template
from flask_security import login_required, current_user
from flask_babel import gettext as _

from vulcan_web.extensions import db
from vulcan_web.blueprints import orders_optimize
from vulcan_web.models import helpers, vulcan
from vulcan_web.libs import optimize


@orders_optimize.route('/', methods=['GET', 'POST'])
@login_required
def index():
    inventory_id = None
    if request.method == "POST":
        inventory_id = int(request.form['inventory_id'])
        inventory = vulcan.Inventory.query.get(inventory_id)
        flash(_(u"Using only selected inventory (%s %dx%dx%d) for optimization") %
              (inventory.ident, inventory.y, inventory.z, inventory.x), 'info')

    pending, created = vulcan.Status.get_or_create(name="Pending")

    results = vulcan.OrderDeliverable.query.with_entities(vulcan.Order).filter(and_(
        vulcan.OrderDeliverable.order_id == vulcan.Order.id,
        vulcan.OrderDeliverable.total > 0
    )).group_by(vulcan.Order.id).all()

    taken = vulcan.Inventory.query.filter(and_(
        vulcan.Inventory.cut_id == vulcan.Cut.id,
        vulcan.Cut.status == pending
    )).count()

    if taken > 0:
        flash(_(u"%d inventory pieces reserved by pending cuts") % taken, 'warning')

    kwargs = {'results': results}
    if inventory_id:
        kwargs['inventory_id'] = inventory_id
    return render_template('orders_optimize.html', **kwargs)


@orders_optimize.route('/details/', methods=['POST'])
@login_required
def details():
    if not request.form['orders']:
        flash(_(u"Please choose the orders for optimization"), 'warning')
        return redirect(url_for('orders.optimize.index'))

    pending, created = vulcan.Status.get_or_create(name="Pending")
    taken = vulcan.Inventory.query.filter(and_(
        vulcan.Inventory.cut_id == vulcan.Cut.id,
        vulcan.Cut.status == pending
    )).count()

    if taken > 0:
        flash(_(u"%d inventory pieces reserved by pending cuts") % taken, 'warning')

    settings = helpers.deep_parse_post('settings', request.form)

    inventory_id = settings.get('inventory_id', None)
    if inventory_id and inventory_id != 'None':
        inventory = vulcan.Inventory.query.get(inventory_id)
        flash(_(u"Using only selected inventory (%s %dx%dx%d) for optimization") %
              (inventory.ident, inventory.y, inventory.z, inventory.x), 'info')

    orders = [int(o) for o in request.form['orders'].split(',')]
    results = vulcan.OrderDeliverable.query.with_entities(vulcan.Order).filter(and_(
        vulcan.OrderDeliverable.order_id == vulcan.Order.id,
        vulcan.Order.id.in_(orders),
        vulcan.OrderDeliverable.total > 0
    )).all()

    kwargs = {
        'results': results,
        'settings': settings,
        'orders': request.form['orders']
    }
    if inventory_id:
        kwargs['inventory_id'] = inventory_id
    return render_template('orders_optimize_details.html', **kwargs)


@orders_optimize.route('/cut/', methods=['POST'])
@login_required
def cut():
    if not request.form['orders']:
        flash(_(u"Please choose the orders for optimization"), 'warning')
        return redirect(url_for('orders.optimize.index'))

    settings = helpers.deep_parse_post('settings', request.form)
    if settings.get('detail', 'off') == 'on':
        return redirect(url_for('orders.optimize.details'), code=307)

    details = helpers.deep_parse_post('details', request.form)
    if details:
        orders = details
    else:
        orders = [int(o) for o in request.form['orders'].split(',')]

    pending, created = vulcan.Status.get_or_create(name="Pending")
    new_cut = vulcan.Cut(
        user=current_user,
        cut_at=datetime.datetime.now(),
        status=pending
    )
    db.session.add(new_cut)
    db.session.commit()

    settings['cid'] = new_cut.id

    success = True
    try:
        optimize.optimize(orders, **settings)
    except optimize.CutException as ce:
        db.session.delete(new_cut)
        flash(ce.message, 'danger')
        success = False
    finally:
        db.session.commit()

    if success:
        return redirect(url_for('cuts.list.detail', cut_id=new_cut.id))

    return redirect(url_for('orders.optimize.index'))
