from flask import jsonify
from flask_babel import gettext as _
from flask_security import login_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import orders_remove
from vulcan_web.models import vulcan


@orders_remove.route('/<int:order_id>/')
@login_required
def index(order_id):
    ret = {
        'id': order_id,
        'message': _(u"Item removed successfully"),
        'success': True
    }

    try:
        ql = vulcan.Order.query.get(order_id)
        db.session.delete(ql)
        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)
