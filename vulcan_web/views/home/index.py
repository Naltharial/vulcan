from flask import render_template
from flask_security import login_required

from vulcan_web.blueprints import home_index


@home_index.route('/')
@login_required
def index():
    return render_template('index.html')
