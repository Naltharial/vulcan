from flask import flash, redirect, request, render_template, url_for
from flask_babel import gettext as _
from flask_security import roles_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import qualities_new
from vulcan_web.models import vulcan


@qualities_new.route('/', methods=['GET', 'POST'])
@roles_required('Admin')
def index():
    data = {
        'name': '',
        'place': '',
        'cut_time': ''
    }
    if request.method == "POST":
        data['name'] = request.form['name']
        data['place'] = request.form['place']
        data['cut_time'] = request.form['cut_time']

        check = True
        if not data['name']:
            flash(_(u"Missing name"), 'danger')
            check = False
        if not data['cut_time']:
            flash(_(u"Missing cut time"), 'danger')
            check = False
        else:
            try:
                int(data['cut_time'])
            except ValueError:
                flash(_(u"Cut time must be an integer"), 'danger')
                check = False

        if check:
            ql = vulcan.Quality(
                name = data['name'],
                place = data['place'],
                cut_time = data['cut_time'],
            )
            db.session.add(ql)
            db.session.commit()

            return redirect(url_for('qualities.list.index'))

    return render_template('qualities_new.html', **data)
