from math import ceil

from flask import request, render_template
from flask_security import login_required

from vulcan_web.blueprints import qualities_list
from vulcan_web.models import helpers, vulcan


@qualities_list.route('/', methods=['GET','POST'])
@login_required
def index():
    query = {}
    if request.args.get('field', None):
        query[request.args.get('field')] = request.args.get('value')

    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    ql = helpers.filter_query(vulcan.Quality.query, **query)
    results = ql.limit(limit).offset(offset).all()
    pages = ceil(float(ql.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page,
        'field': None
    }
    if request.args.get('field', None):
        data['field'] = request.args.get('field')
        data['value'] = request.args.get('value')

    return render_template('qualities_list.html', **data)
