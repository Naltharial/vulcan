from flask import jsonify
from flask_babel import gettext as _
from flask_security import login_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import qualities_remove
from vulcan_web.models import vulcan


@qualities_remove.route('/<int:quality_id>/')
@login_required
def index(quality_id):
    ret = {
        'id': quality_id,
        'message': _(u'Item removed successfully'),
        'success': True
    }

    try:
        ql = vulcan.Quality.query.get(quality_id)
        db.session.delete(ql)
        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)
