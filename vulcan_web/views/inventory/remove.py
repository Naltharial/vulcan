from flask import jsonify
from flask_babel import gettext as _
from flask_security import login_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import inventory_remove
from vulcan_web.models import vulcan


@inventory_remove.route('/<int:inventory_id>/')
@login_required
def index(inventory_id):
    ret = {
        'id': inventory_id,
        'message': _(u'Item removed successfully'),
        'success': True
    }

    try:
        ql = vulcan.Inventory.query.get(inventory_id)
        db.session.delete(ql)
        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)
