from flask import redirect, request, render_template, url_for
from flask_security import login_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import inventory_clear
from vulcan_web.models import vulcan


@inventory_clear.route('/', methods=['GET', 'POST'])
@login_required
def index():
    if request.method == "POST":
        if request.form['action'] == "clear":
            vulcan.Inventory.query.delete()
            db.session.commit()

            return redirect(url_for('inventory.list.index'))

    return render_template('inventory_clear.html')
