from math import ceil

from flask import request, render_template, flash
from flask_security import login_required
from flask_babel import gettext as _

from vulcan_web.extensions import db
from vulcan_web.blueprints import inventory_list
from vulcan_web.models import helpers, vulcan


@inventory_list.route('/')
@login_required
def index():
    query = {
        'active': True
    }

    if request.args.get('field', None):
        query[request.args.get('field')] = request.args.get('value').strip()

    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    ql = helpers.filter_query(vulcan.Inventory.query, **query).filter(vulcan.Inventory.cut_id.is_(None))
    results = ql.limit(limit).offset(offset).all()
    pages = ceil(float(ql.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page,
        'field': None
    }
    if request.args.get('field', None):
        data['field'] = request.args.get('field')
        data['value'] = request.args.get('value')

    return render_template('inventory_list.html', **data)


@inventory_list.route('/<int:inventory_id>/', methods=['GET', 'POST'])
@login_required
def detail(inventory_id):
    result = vulcan.Inventory.query.get(inventory_id)

    data = {}
    if request.method == 'POST':
        data['x'] = request.form['x'] or result.x
        data['y'] = request.form['y'] or result.y
        data['z'] = request.form['z'] or result.z
        data['place'] = request.form['place'] or result.place

        check = True
        try:
            int(data['x'])
        except ValueError:
            flash(_(u"Length must be an integer"), 'danger')
            check = False
        try:
            int(data['y'])
        except ValueError:
            flash(_(u"Height must be an integer"), 'danger')
            check = False
        try:
            int(data['z'])
        except ValueError:
            flash(_(u"Width must be an integer"), 'danger')
            check = False

        if check:
            result.x = int(data['x'])
            result.y = int(data['y'])
            result.z = int(data['z'])
            result.place = data['place']
            result.manual = True

            db.session.add(result)
            db.session.commit()

    return render_template('inventory_detail.html', result=result)
