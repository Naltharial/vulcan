import datetime
from io import BytesIO
from StringIO import StringIO
from openpyxl import load_workbook

from flask import flash, get_flashed_messages, redirect, request, render_template, url_for, send_file
from flask_babel import gettext as _
from flask_security import login_required

from openpyxl import Workbook
from openpyxl.styles import Border, Side, PatternFill, NamedStyle
from openpyxl.utils import get_column_letter

from vulcan_web.extensions import db
from vulcan_web.blueprints import inventory_iex
from vulcan_web.models import vulcan


@inventory_iex.route('/')
@login_required
def index():
    flash(get_flashed_messages())
    return render_template('inventory_iex.html')


@inventory_iex.route('/import/', methods=['POST'])
@login_required
def iexi():
    ifile = request.files['import']
    if not ifile.filename:
        flash(_(u'Please select a file.'), 'danger')
        return redirect(url_for('inventory.iex.index'))

    wb = load_workbook(filename=BytesIO(ifile.read()), read_only=True)

    qlmap = {}
    objects = []
    with db.session.no_autoflush:
        for sheet in wb:
            first = True
            for row in sheet.rows:
                obj = {}
                if first:
                    first = False
                    continue

                # Wrong worksheet
                if not row[0].value:
                    break

                if len(row) < 9 or not row[0].value or not row[2].value or not row[4].value or \
                        not row[6].value or not row[7].value or not row[8].value:
                    flash(_(u"Inventory entry with ident '%s' error - missing attributes") %
                                             (row[0].value,), 'warning')
                    continue

                qln = str(row[7].value).strip()
                if qln not in qlmap:
                    quality = vulcan.Quality.query.filter_by(name=row[7].value).first()
                    if quality:
                        qlmap[qln] = quality.id
                    else:
                        flash(_(u"Inventory entry with ident '%s' error - quality '%s' does not exist") %
                                                 (row[0].value, row[7].value), 'warning')
                        continue

                if len(row) > 13 and row[13].value is not None and row[13].value.strip():
                    elem = vulcan.Inventory.query.get(row[13].value)
                    if elem is not None:
                        obj['id'] = row[13].value

                obj['ident'] = row[0].value
                obj['y'] = row[2].value
                obj['z'] = row[4].value
                obj['x'] = row[6].value
                obj['quality_id'] = qlmap[qln]
                obj['place'] = row[8].value
                obj['received_at'] = datetime.datetime.now()
                obj['active'] = 1
                obj['manual'] = 1

                objects.append(obj)

    db.session.bulk_insert_mappings(vulcan.Inventory, objects)
    db.session.commit()

    return redirect(url_for('inventory.list.index'))


@inventory_iex.route('/export/', methods=['POST'])
@login_required
def iexe():
    wb = Workbook()
    ws = wb.active

    gray_back = PatternFill(patternType='lightGray')
    bold_border = Border(left=Side(style='medium'),
                         right=Side(style='medium'),
                         top=Side(style='medium'),
                         bottom=Side(style='medium'))
    fborder_style = NamedStyle(name='fborder', fill=gray_back, border=bold_border)

    begin_r = ws.max_row
    begin_c = get_column_letter(1)
    end_c = get_column_letter(14)

    ws.append([
        _("ident"),
        "",
        _("height"),
        "",
        _("width"),
        "",
        _("length"),
        _("quality"),
        _("place"),
        _("weight"),
        _("date"),
        "",
        _("usage"),
        "VID"
    ])
    end_r = ws.max_row
    range_c = '%s%d:%s%d' % (begin_c, begin_r, end_c, end_r,)
    for row in ws[range_c]:
        for cell in row:
            cell.style = fborder_style

    elements = vulcan.Inventory.query.all()

    for element in elements:
        el_date = datetime.date.today().strftime("%d.%m.%Y")
        if element.received_at:
            el_date = element.received_at.strftime("%d.%m.%Y")

        ws.append([
            element.ident,
            "",
            element.y,
            "",
            element.z,
            "",
            element.x,
            element.quality.name,
            element.place,
            element.weight,
            el_date,
            "",
            "",
            element.id
        ])

    out = StringIO()
    wb.save(out)
    out.seek(0)

    filename = u'%s-%s.xlsx' % (_(u"Inventory"), datetime.date.today().isoformat())

    return send_file(out, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            attachment_filename=filename, as_attachment=True)
