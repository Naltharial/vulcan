from math import ceil

from flask import flash, current_app, jsonify, request, redirect, render_template, url_for
from flask_babel import gettext as _
from flask_security import roles_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import users_roles
from vulcan_web.models import helpers, security


@users_roles.route('/', methods=['GET', 'POST'])
@roles_required('Admin')
def index():
    query = {}
    if request.method == 'POST':
        query[request.form['field']] = request.form['value']

    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    ql = helpers.filter_query(security.Role.query, **query)
    results = ql.limit(limit).offset(offset).all()
    pages = ceil(float(ql.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page,
        'field': None
    }
    if request.method == 'POST':
        data['field'] = request.form['field']
        data['value'] = request.form['value']

    return render_template('roles_list.html', **data)


@users_roles.route('/new/', methods=['GET', 'POST'])
@roles_required('Admin')
def new():
    data = {
        'name': '',
        'description': '',
    }
    if request.method == "POST":
        data['name'] = request.form['name']
        data['description'] = request.form['description']

        check = True
        if not data['name']:
            flash(_(u"Missing name"), 'danger')
            check = False
        if not data['description']:
            flash(_(u"Missing description"), 'danger')
            check = False

        if check:
            order = security.Role(
                name = data['name'],
                description = data['description'],
            )

            db.session.add(order)
            db.session.commit()

            return redirect(url_for('users.roles.index'))

    return render_template('roles_new.html', **data)


@users_roles.route('/<int:role_id>/add_user/<int:user_id>/')
@roles_required('Admin')
def add_user(role_id, user_id):
    ret = {
        'id': role_id,
        'message': _(u'Role added successfully'),
        'success': True
    }

    try:
        user = current_app.user_datastore.get_user(user_id)
        role = security.Role.query.get(role_id)
        current_app.user_datastore.add_role_to_user(user, role)

        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)


@users_roles.route('/<int:role_id>/remove_user/<int:user_id>/')
@roles_required('Admin')
def remove_user(role_id, user_id):
    ret = {
        'id': role_id,
        'message': _(u'Role removed successfully'),
        'success': True
    }

    try:
        user = current_app.user_datastore.get_user(user_id)
        role = security.Role.query.get(role_id)
        current_app.user_datastore.remove_role_from_user(user, role)

        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)
