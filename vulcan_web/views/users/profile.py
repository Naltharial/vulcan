from flask import current_app, jsonify, request
from flask_babel import gettext as _
from flask_security import login_required, current_user

from vulcan_web.extensions import db
from vulcan_web.blueprints import users_profile


@users_profile.route('/language/', methods=['POST'])
@login_required
def change_language():
    lang_id = request.form['lang_id']
    ret = {
        'lang': lang_id,
        'message': _(u'Language switched successfully'),
        'success': True
    }

    if lang_id in current_app.config['BABEL_LANGUAGES']:
        current_user.locale = lang_id
        db.session.commit()
    else:
        ret['success'] = False
        ret['message'] = _(u'Language not recognized')

    return jsonify(**ret)
