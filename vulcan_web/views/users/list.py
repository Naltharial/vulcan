from math import ceil

from flask import current_app, request, render_template
from flask_security import roles_required

from vulcan_web.blueprints import users_list
from vulcan_web.models import helpers, security


@users_list.route('/', methods=['GET', 'POST'])
@roles_required('Admin')
def index():
    query = {}
    if request.args.get('field', None):
        query[request.args.get('field')] = request.args.get('value')

    page = int(request.args.get('page', 0))
    limit = int(request.args.get('limit', 16))
    offset = page * limit

    ql = helpers.filter_query(security.User.query, **query)
    results = ql.limit(limit).offset(offset).all()
    pages = ceil(float(ql.count()) / limit)
    data = {
        'results': results,
        'pages': int(pages),
        'current': page,
        'field': None
    }
    if request.args.get('field', None):
        data['field'] = request.args.get('field')
        data['value'] = request.args.get('value')

    return render_template('users_list.html', **data)


@users_list.route('/<int:user_id>/')
@roles_required('Admin')
def detail(user_id):
    result = current_app.user_datastore.get_user(user_id)
    roles = security.Role.query.all()
    return render_template('users_detail.html', result=result, roles=roles)
