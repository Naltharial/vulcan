from flask import flash, redirect, request, render_template, url_for, current_app
from flask_babel import gettext as _
from flask_security import roles_required
from flask_security.utils import encrypt_password

from vulcan_web.extensions import db
from vulcan_web.blueprints import users_new


@users_new.route('/', methods=['GET', 'POST'])
@roles_required('Admin')
def index():
    data = {
        'email': '',
        'password': '',
    }
    if request.method == "POST":
        data['email'] = request.form['email']
        data['password'] = request.form['password']

        check = True
        if not data['email']:
            flash(_(u"Missing email"), 'danger')
            check = False
        if not data['password']:
            flash(_(u"Missing password"), 'danger')
            check = False

        if check:
            current_app.user_datastore.create_user(email=data['email'], password=encrypt_password(data['password']))
            db.session.commit()

            return redirect(url_for('users.list.index'))

    return render_template('users_new.html', **data)
