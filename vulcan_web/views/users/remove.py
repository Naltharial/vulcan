from flask import jsonify, current_app
from flask_babel import gettext as _
from flask_security import roles_required

from vulcan_web.extensions import db
from vulcan_web.blueprints import users_remove


@users_remove.route('/<int:user_id>/')
@roles_required('Admin')
def index(user_id):
    ret = {
        'id': user_id,
        'message': _(u'Item removed successfully'),
        'success': True
    }

    try:
        user = current_app.user_datastore.get_user(user_id)
        current_app.user_datastore.delete_user(user)
        db.session.commit()

        return jsonify(**ret)

    except Exception as e:
        ret['success'] = False
        ret['message'] = e.message

        return jsonify(**ret)
