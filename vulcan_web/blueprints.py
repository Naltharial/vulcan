"""All Flask blueprints for the entire application.

All blueprints for all views go here. They shall be imported by the views themselves and by application.py. Blueprint
URL paths are defined here as well.
"""

from flask import Blueprint


def _factory(partial_module_string, url_prefix):
    """Generates blueprint objects for view modules.

    Positional arguments:
    partial_module_string -- string representing a view module without the absolute path (e.g. 'home.index' for
        vulcan_web.views.home.index).
    url_prefix -- URL prefix passed to the blueprint.

    Returns:
    Blueprint instance for a view module.
    """
    name = partial_module_string
    import_name = 'vulcan_web.views.{}'.format(partial_module_string)
    template_folder = 'templates'
    blueprint = Blueprint(name, import_name, template_folder=template_folder, url_prefix=url_prefix)
    return blueprint

# TODO: see vulcan_web.core.menu
home_index = _factory('home.index', '/')

cuts_list = _factory('cuts.list', '/cuts/list')

inventory_list = _factory('inventory.list', '/inventory/list')
inventory_iex = _factory('inventory.iex', '/inventory/iex')
inventory_clear = _factory('inventory.clear', '/inventory/clear')
inventory_remove = _factory('inventory.remove', '/inventory/remove')

orders_list = _factory('orders.list', '/orders/list')
orders_new = _factory('orders.new', '/orders/new')
orders_iex = _factory('orders.iex', '/orders/iex')
orders_optimize = _factory('orders.optimize', '/orders/optimize')
orders_remove = _factory('orders.remove', '/orders/remove')

qualities_list = _factory('qualities.list', '/qualities/list')
qualities_new = _factory('qualities.new', '/qualities/new')
qualities_remove = _factory('qualities.remove', '/qualities/remove')

users_list = _factory('users.list', '/users/list')
users_new = _factory('users.new', '/users/new')
users_roles = _factory('users.roles', '/users/roles')
users_remove = _factory('users.remove', '/users/remove')
users_profile = _factory('users.profile', '/users/profile')

all_blueprints = (
    home_index,

    cuts_list,

    inventory_list,
    inventory_iex,
    inventory_clear,
    inventory_remove,

    orders_list,
    orders_new,
    orders_iex,
    orders_optimize,
    orders_remove,

    qualities_list,
    qualities_new,
    qualities_remove,

    users_list,
    users_new,
    users_roles,
    users_remove,
    users_profile,
)
