"""Holds the create_app() Flask application factory. More information in create_app() docstring."""

from importlib import import_module
import locale
import os
import sys
from yaml import load

from flask import g, request
from flask import Flask
from flask_statics import Statics
from flask_babel import Babel
from flask_security import Security, SQLAlchemyUserDatastore, current_user

import vulcan_web as app_root
from vulcan_web.core.menu import get_menu
from vulcan_web.blueprints import all_blueprints
from vulcan_web.extensions import celery, db, mail, redis
from vulcan_web.models import security as security_models

reload(sys)
sys.setdefaultencoding('utf-8')

APP_ROOT_FOLDER = os.path.abspath(os.path.dirname(app_root.__file__))
TEMPLATE_FOLDER = os.path.join(APP_ROOT_FOLDER, 'templates')
STATIC_FOLDER = os.path.join(APP_ROOT_FOLDER, 'static')
REDIS_SCRIPTS_FOLDER = os.path.join(APP_ROOT_FOLDER, 'redis_scripts')


def get_config(config_class_string, yaml_files=None):
    """Load the Flask config from a class.

    Positional arguments:
    config_class_string -- string representation of a configuration class that will be loaded (e.g.
        'vulcan_web.config.Production').
    yaml_files -- List of YAML files to load. This is for testing, leave None in dev/production.

    Returns:
    A class object to be fed into app.config.from_object().
    """
    config_module, config_class = config_class_string.rsplit('.', 1)
    config_class_object = getattr(import_module(config_module), config_class)
    config_obj = config_class_object()

    # Expand some options.
    celery_fmt = 'vulcan_web.tasks.{}'
    db_fmt = 'vulcan_web.models.{}'
    if getattr(config_obj, 'CELERY_IMPORTS', False):
        config_obj.CELERY_IMPORTS = [celery_fmt.format(m) for m in config_obj.CELERY_IMPORTS]
    for definition in getattr(config_obj, 'CELERYBEAT_SCHEDULE', dict()).values():
        definition.update(task=celery_fmt.format(definition['task']))
    if getattr(config_obj, 'DB_MODELS_IMPORTS', False):
        config_obj.DB_MODELS_IMPORTS = [db_fmt.format(m) for m in config_obj.DB_MODELS_IMPORTS]
    #for script_name, script_file in getattr(config_obj, 'REDIS_SCRIPTS', dict()).items():
    #    config_obj.REDIS_SCRIPTS[script_name] = os.path.join(REDIS_SCRIPTS_FOLDER, script_file)

    # Load additional configuration settings.
    yaml_files = yaml_files or [f for f in [
        os.path.join('/', 'etc', 'vulcan_web', 'config.yml'),
        os.path.abspath(os.path.join(APP_ROOT_FOLDER, '..', 'config.yml')),
        os.path.join(APP_ROOT_FOLDER, 'config.yml'),
    ] if os.path.exists(f)]
    additional_dict = dict()
    for y in yaml_files:
        with open(y) as f:
            additional_dict.update(load(f.read()))

    # Merge the rest into the Flask app config.
    for key, value in additional_dict.items():
        setattr(config_obj, key, value)

    return config_obj


def create_app(config_obj, no_sql=False):
    """Flask application factory. Initializes and returns the Flask application.

    Blueprints are registered in here.

    Modeled after: http://flask.pocoo.org/docs/patterns/appfactories/

    Positional arguments:
    config_obj -- configuration object to load into app.config.

    Keyword arguments:
    no_sql -- does not run init_app() for the SQLAlchemy instance. For Celery compatibility.

    Returns:
    The initialized Flask application.
    """
    # Initialize app. Flatten config_obj to dictionary (resolve properties).
    app = Flask(__name__, template_folder=TEMPLATE_FOLDER, static_folder=STATIC_FOLDER)
    config_dict = dict([(k, getattr(config_obj, k)) for k in dir(config_obj) if not k.startswith('_')])
    app.config.update(config_dict)

    # Import DB models. Flask-SQLAlchemy doesn't do this automatically like Celery does.
    mixins = []
    with app.app_context():
        for module in app.config.get('DB_MODELS_IMPORTS', list()):
            mod = import_module(module)

            pm = getattr(mod, 'property_mixins', None)
            if pm:
                mixins.append(pm)

    # Setup redirects and register blueprints.
    app.add_url_rule('/favicon.ico', 'favicon', lambda: app.send_static_file('favicon.ico'))
    for bp in all_blueprints:
        import_module(bp.import_name)
        app.register_blueprint(bp)

    # Initialize extensions/add-ons/plugins.
    if not no_sql:
        db.init_app(app)
    Statics(app)  # Enable Flask-Statics-Helper features.

    app.user_datastore = SQLAlchemyUserDatastore(db, security_models.User, security_models.Role)
    Security(app, app.user_datastore)

    @app.context_processor
    def inject_user():
        g.user = current_user
        return {}

    redis.init_app(app)
    celery.init_app(app)
    mail.init_app(app)

    # Activate middleware.
    with app.app_context():
        import_module('vulcan_web.middleware')

    app.babel = Babel(app)

    @app.babel.localeselector
    def get_locale():
        # if a user is logged in, use the locale from the user settings
        user = current_user
        if user is not None and hasattr(user, 'locale'):
            return user.locale

        return request.accept_languages.best_match(app.config['BABEL_LANGUAGES'])

    @app.babel.timezoneselector
    def get_timezone():
        user = getattr(g, 'user', None)
        if user is not None and hasattr(user, 'timezone'):
            return user.timezone
        else:
            return 'CET'

    @app.context_processor
    def build_menu():
        menu, ordering = get_menu()
        return dict(menu=menu, menu_order=ordering)

    @app.context_processor
    def build_bc():
        bc = []
        if request.blueprint:
            bc = [w.capitalize() for w in request.blueprint.split('.')]
        return dict(breadcrumbs=bc)

    @app.template_filter('min')
    def list_min(lst):
        return min(lst)

    @app.template_filter('max')
    def list_max(lst):
        return max(lst)

    @app.before_first_request
    def create_users():
        role = app.user_datastore.find_or_create_role("Admin", description="Vulcan Administrator")
        if security_models.User.query.count() == 0:
            user = app.user_datastore.create_user(email=app.config.get('ADMINS', ['dev@null.test'])[0], password='admin')
            app.user_datastore.add_role_to_user(user, role)
        db.session.commit()

    @app.before_first_request
    def activate_mixins():
        for mixin in mixins:
            mixin()

    # Return the application instance.
    return app
