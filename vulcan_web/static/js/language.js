$(document).ready(function() {
    $('article.language select').on('change', function() {
        $.ajax({
            type: "POST",
            url: "/users/profile/language/",
            data: {
                'lang_id' : $(this).val()
            },
            success: function (data) {
                if (data.success) {
                    location.reload(true)
                }
            },
            dataType: "json"
        });
    });
});
