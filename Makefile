PYCODE = import flask_statics as a; print a.__path__[0]
.PHONY: default isvirtualenv

default:
	@echo "Local examples:"
	@echo "    make run        # Starts a Flask development server locally."
	@echo "    make shell      # Runs 'manage.py shell' locally with iPython."
	@echo "    make celery     # Runs one development Celery worker with Beat."
	@echo "    make style      # Check code styling with flake8."
	@echo "    make lint       # Runs PyLint."
	@echo "    make test       # Tests entire application with pytest."

isvirtualenv:
	@if [ -z "$(VIRTUAL_ENV)" ]; then echo "ERROR: Not in a virtualenv." 1>&2; exit 1; fi

run:
	(((i=0; while \[ $$i -lt 40 \]; do sleep 0.5; i=$$((i+1)); \
		netstat -anp tcp |grep "\.5000.*LISTEN" &>/dev/null && break; done) && open http://localhost:5000/) &)
	./manage.py devserver

shell:
	./manage.py shell

celery:
	./manage.py celerydev

style:
	flake8 --max-line-length=120 --statistics vulcan_web

lint:
	pylint --max-line-length=120 vulcan_web

test:
	py.test --cov-report term-missing --cov vulcan_web tests

testpdb:
	py.test --pdb tests

testcovweb:
	py.test --cov-report html --cov vulcan_web tests
	open htmlcov/index.html

pipinstall: isvirtualenv
	# For development environments. Symlinks are for PyCharm inspections to work with Flask-Statics-Helper.
	pip install -r requirements.txt flake8 pylint ipython pytest-cov
	[ -h vulcan_web/templates/flask_statics_helper ] || ln -s `python -c "$(PYCODE)"`/templates/flask_statics_helper \
		vulcan_web/templates/flask_statics_helper
	[ -h vulcan_web/static/flask_statics_helper ] || ln -s `python -c "$(PYCODE)"`/static \
		vulcan_web/static/flask_statics_helper

