import sys
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from vulcan_web.application import create_app, get_config
from vulcan_web.extensions import db


def parse_options():
    """Parses command line options for Flask.

    Returns:
    Config instance to pass into create_app().
    """
    # Figure out which class will be imported.
    if '--config' in sys.argv:
        loc = sys.argv.index('--config')
        val = sys.argv[loc+1]
        sys.argv = sys.argv[0:loc] + sys.argv[loc+1:len(sys.argv)-1]
        config_class_string = 'vulcan_web.config.' + val
    else:
        config_class_string = 'vulcan_web.config.Config'
    config_obj = get_config(config_class_string)

    return config_obj

app = create_app(parse_options())

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
