"""Separate out orders to cuts

Revision ID: cc9bac272e9d
Revises: b50586d99fcf
Create Date: 2016-11-07 22:47:20.145000

"""

# revision identifiers, used by Alembic.
revision = 'cc9bac272e9d'
down_revision = 'b50586d99fcf'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_table('element_results')
    op.drop_table('elements')

    op.drop_constraint('orders_ibfk_1', 'orders', type_="foreignkey")
    op.drop_column('orders', 'cut_id')

    op.create_table(
        'statuses',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(45)),
        sa.UniqueConstraint('name', name="uq_status_domain"),
        mysql_engine='InnoDB'
    )
    op.drop_column('cuts', 'activated')
    op.execute('INSERT INTO statuses (id, name) VALUES (1, "Pending"), (2, "Rejected"), (3, "Approved")')
    op.add_column('cuts', sa.Column('status_id', sa.Integer,
                                    sa.ForeignKey('statuses.id', name='fk_cut_status'),
                                    nullable=False, server_default='1', index=True))

    op.create_table(
        'order_deliverable_cuts',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('order_deliverable_id', sa.Integer,
                  sa.ForeignKey('order_deliverables.id', name='fk_deliverablecut_deliverable',
                                onupdate='CASCADE', ondelete='CASCADE'),
                  nullable=False, index=True),
        sa.Column('cut_id', sa.Integer,
                  sa.ForeignKey('cuts.id', name='fk_deliverablecut_cut',
                                onupdate='CASCADE', ondelete='CASCADE'),
                  nullable=False, index=True),
        sa.Column('amount', sa.Integer, nullable=False),
        sa.UniqueConstraint('order_deliverable_id', 'cut_id', name="uq_deliverablecut_domain"),
        mysql_engine='InnoDB'
    )


def downgrade():
    op.drop_table('order_deliverable_cuts')

    op.add_column('cuts', sa.Column('activated', sa.Boolean, nullable=False, default='0'))

    op.drop_constraint('fk_cut_status', 'cuts', type_="foreignkey")
    op.drop_column('cuts', 'status_id')

    op.drop_table('statuses')

    op.add_column('orders', sa.Column('cut_id', sa.Integer,
                                      sa.ForeignKey('cuts.id', name='orders_ibfk_1',
                                                    onupdate='SET NULL', ondelete='SET NULL')))

    op.create_table(
        'elements',
        sa.Column('id', sa.Integer(), nullable=True),
        sa.Column('cut_id', sa.Integer(), nullable=False),
        sa.Column('inventory_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['cut_id'], ['cuts.id'], onupdate='CASCADE', ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['inventory_id'], ['inventory.id'], ),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )
    op.create_table(
        'element_results',
        sa.Column('id', sa.Integer(), nullable=True),
        sa.Column('element_id', sa.Integer(), nullable=False),
        sa.Column('inventory_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['element_id'], ['elements.id'], onupdate='CASCADE', ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['inventory_id'], ['inventory.id'], ),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )
